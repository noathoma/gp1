using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//all jons

//makes the class show up in inspectorino
[System.Serializable] public class SpriteAnim {
	public Sprite[] frames;
	public float animSpeed = 15;
	public Sprite Frame(float time) {
		if(frames == null || frames.Length == 0){return null;}
		int frame = ((int) time) % frames.Length;
		return frames[frame];
	}

}

public class Movement : CharacterController2D {

	public float speed = 7f;
	public float jumpForce = 15f;

	public float gravity = 33;
	public Vector3 velocity = Vector3.zero;
	public float skinWidth = .01f / 16;

	public bool onGround { get { return IsTouching(Vector3.down * skinWidth); } }
	public bool onHead { get { return IsTouching(Vector3.up * skinWidth); } }

	public SpriteAnim idle;
	public SpriteAnim move;
	public SpriteAnim jump;
	public SpriteAnim fall;
	public SpriteAnim crouch;

	// Start is called before the first frame update
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		// anti speedrun
		if (Time.deltaTime > .1) { return; }

		Vector3 input = Vector3.zero;
		if (Input.GetKey("left")) { input.x -= 1; }
		if (Input.GetKey("right")) { input.x += 1; }
		if (Input.GetKey("down")){input.y -= 1; }

		if (onGround) {
			velocity.y = 0;

			if (Input.GetKeyDown("up")||Input.GetKeyDown("space")) {
				velocity.y = jumpForce;
			}
		} else {
			if(velocity.y > 0 ) {
				if(onHead || Input.GetKeyUp("up")||Input.GetKeyUp("space")){
					velocity.y = 0;
				}
			}
			velocity.y -= gravity * Time.deltaTime;


		}

		Vector3 movement = Vector3.zero;
		movement += input * speed;
		movement += velocity;
		Vector3 moved = Move(movement * Time.deltaTime);

		UpdateAnimation (moved, input);
	}

	SpriteRenderer spr;
	SpriteAnim currentAnim;
	SpriteAnim lastAnim;
	float animTime;

	void UpdateAnimation(Vector3 moved, Vector3 input) {
		if(spr == null){spr = GetComponent<SpriteRenderer>();}
		if(spr == null){return ;}

		if(input.x >0){spr.flipX = false;}
		if(input.x <0){spr.flipX = true;}

		if (onGround) {
			
			if (Mathf.Abs(moved.x) > 0) {
				currentAnim = move;
			} else {
				currentAnim = idle;
				if (input.y < 0) {//i'm going to eventually be 
					currentAnim = crouch;
				}
			}
		} else {
			if(velocity.y > 0) {
				currentAnim = jump;
			} else {
				currentAnim = fall;
			}
		}
		animTime += currentAnim.animSpeed * Time.deltaTime; 
		if(lastAnim != currentAnim){animTime = 0;}
		spr.sprite = currentAnim.Frame(animTime);
		
		//reset every animation
		lastAnim = currentAnim;


	}
}
