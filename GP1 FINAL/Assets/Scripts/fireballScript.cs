using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireballScript : MonoBehaviour {
    // Start is called before the first frame update
    public GameObject fireballPrefab;
    protected GameObject fireball;
    public float moveX;
    public float distance;

    void Start() {
        InvokeRepeating("LaunchProjectile", 0.0f, 5.0f);
    }

    // Update is called once per frame
    void Update() {
        if (fireball != null) {
            if (fireball.transform.position.x < distance) {
                Destroy(fireball);
            } else {
                fireball.transform.position -= new Vector3(moveX * Time.deltaTime, 0, 0);
            }
        }
    }

    void LaunchProjectile() {
        fireball = Instantiate<GameObject>(fireballPrefab);
    }
}