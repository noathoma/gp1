using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class playerMovement : MonoBehaviour
{

    public float maxSpeed;
    public float speed;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //finds a component of type Animator that's attached to the gameObject that ownss this script
        animator = GetComponent<Animator>();

        if (animator == null)
        {
            Debug.LogError("No animator attached");
        }
       // playerBase = gameObject.GetComponent<Player_Base>();
      //  rigidbody2D = transform.GetComponent<rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        float horizontalAxis = Input.GetAxis("Horizontal");
        // float verticalAxis = Input.GetAxis("Vertical");

        //check if controller is pushed left or right
        if (Mathf.Abs(horizontalAxis) > 0.0001f)
        {
            speed = maxSpeed * horizontalAxis * Time.deltaTime;

            transform.Translate(speed, 0.0f, 0.0f);

            //animator.SetBool("isRunning", true);


        }
        else
        {
            //animator.SetBool("isRunning", false);
        }
    }
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 5f), ForceMode2D.Impulse);
        }


    }
}
