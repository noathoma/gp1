using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moonyscript : MonoBehaviour
{
    public float speed;
    public float angle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.GetChild(0).RotateAround(transform.position, Vector3.forward, angle * speed * Time.deltaTime);
        transform.GetChild(1).RotateAround(transform.position, Vector3.forward, angle * speed * Time.deltaTime);
    }
}
