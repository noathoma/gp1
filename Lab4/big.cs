using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class big : MonoBehaviour
{
    public float angle;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.GetChild(0).RotateAround(transform.position, Vector3.forward, angle * speed * Time.deltaTime);
    }
}
