using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//created by Noah Thomas, helped by Dori Othman. Basically she helped me by telling me the basic theories of what I was supposed to be doing because I wanted to do it completely wrong.
public class moveScript : MonoBehaviour
{
    public Vector3 start;
    public Vector3 stop;
    private Vector3 target;

    // Start is called before the first frame update
    void Start()
    {
        target = stop;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        gameManagerScript sm = obj.GetComponent<gameManagerScript>();

        //speed calc
        float stepMove = sm.movementSpeed * Time.deltaTime; // distance move calc
        float stepRotate = sm.rotationSpeed * Time.deltaTime; //rotate distance calc
        float stepScale = sm.scalingSpeed * Time.deltaTime; // scale distance calc

        //move to target
        transform.position = Vector3.MoveTowards(transform.position, target, stepMove);
        transform.Rotate(new Vector3(0, 0, stepRotate));
        transform.localScale += new Vector3(stepScale, stepScale, 0);

        //change target pos
        if (transform.position == stop)
        {
            target = start;
            sm.scalingSpeed *= -1.0f;
        }
        if(transform.position == start)
        {
            target = stop;
            sm.scalingSpeed *= -1.0f;
        }
    }
}
