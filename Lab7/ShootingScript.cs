using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{
    public float speed;
    Rigidbody2D rb;

    void Start(){
        rb = GetComponent<Rigidbody2D>();
            
    }

    // Update is called once per frame
    void FixedUpdate(){
        rb.velocity = transform.right * speed;
    }
    void OnTriggerEnter2D(Collider2D hitInfo){
        if(hitInfo.gameObject.tag == "Kinematic"){
            Debug.Log ("kinematic");
        }
        if(hitInfo.gameObject.tag == "Finish"){
            Debug.Log("Static");
            Destroy(gameObject);
        }

         
    }
}
