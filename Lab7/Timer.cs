using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour{
    public Transform startPos;
    public GameObject pellet;
    float fireSpeed;
    public float startFireSpeed;
    void Start(){
        fireSpeed = startFireSpeed;
    }
    void Update(){
        if(fireSpeed<=0){
            Instantiate(pellet,startPos.position,startPos.rotation);
            fireSpeed = startFireSpeed;
        }
        else{
            fireSpeed -= Time.deltaTime;
        }
    }



}
