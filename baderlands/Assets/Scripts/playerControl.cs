using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerControl : MonoBehaviour {
	public int itemPick = 0;
	public float moveSpeed;
	public float jumpForce;
	public Transform ceilingCheck;
	public Transform groundCheck;
	public LayerMask groundObjects;  
	public float checkRadius;
	public int maxJumpCount;
	public int health;

	private Rigidbody2D rb;
	private bool isRight = true;
	private float moveDir;
	private bool isJumping = false;
	private bool isGrounded;
	private int jumpCount; 
	
	//for component referencing
	private void Awake() {
		rb = GetComponent<Rigidbody2D>(); // looks for the rigid body
	}
	public void Start() {
		jumpCount = maxJumpCount;
	}// Update is called once per frame
	void Update() {
		ProcessInputs();
		Animate();
	}
	public void TakeDamage(int damage) {
		health -= damage;
		if(health <= 0) {
			Die();
		}
	}
	public void ItemPickup(){
		itemPick++;
		Debug.Log("asdfjkl;asdfjkl;asdfjkl;asdfjkl;asdfjkl;asdf;jklasdfjkl;asdf");
		if(itemPick >0){
			Debug.Log("number up");
		}
	}
	void Die() {
		//Instantiate(deathEffect, transform.position, Quaternion.identity);
		 SceneManager.LoadScene("Start Menu");
	}
	private void FixedUpdate() {
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundObjects);
		if (isGrounded) {
			jumpCount = maxJumpCount;
		}
		Move();
	}
	private void Move() {
		rb.velocity = new Vector2(moveDir * moveSpeed, rb.velocity.y);
		if (isJumping) {
			rb.AddForce(new Vector2(0f, jumpForce));
			jumpCount--;
		}
		isJumping = false;
	}
	private void Animate() {
		if (moveDir > 0 && !isRight) {
			FlipCharacter();
		} else if (moveDir < 0 && isRight) {
			FlipCharacter();
		}
	}
	private void ProcessInputs() {
		moveDir = Input.GetAxis("Horizontal");//-1 to 1
		if (Input.GetButtonDown("Jump") && jumpCount > 0 ) {
			isJumping = true;
		}
	}
	private void FlipCharacter() {
		isRight = !isRight;
		transform.Rotate(0f, 180f, 0f);
	}
}
