using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	public float projectileSpeed;
	public GameObject impact;

	private Rigidbody2D rb;

	// Start is called before the first frame update
	void Start() {
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = transform.right * projectileSpeed;
	}
	public void OnTriggerEnter2D(Collider2D collision) {
		Instantiate(impact, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
	// Update is called once per frame
	void Update() {

	}

}
