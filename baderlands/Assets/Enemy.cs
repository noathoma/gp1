using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public int health;
	//public GameObject deathEffect;
 	 public playerControl pc;
	public void TakeDamage(int damage) {
		if(pc.itemPick != 0){
		health -= damage;
		if(health <= 0) {
			Die();
		}
		}
	}
	void Die() {
		//Instantiate(deathEffect, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
