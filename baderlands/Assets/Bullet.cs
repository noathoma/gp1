using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float handSpeed;
	public Rigidbody2D rb;
	public int damage = 30;
	//public GameObject enemyExplode;

	// Start is called before the first frame update
	void Start() {
		rb.velocity = transform.right * handSpeed;
	}
	void OnTriggerEnter2D(Collider2D hitInfo) {
		Enemy enemy = hitInfo.GetComponent<Enemy>();
		if(enemy != null) {
			enemy.TakeDamage(damage);
		}
		//Instantiate(enemyExplode, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}
