using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigEnemy : MonoBehaviour {
	public int health;
	//public GameObject deathEffect;
    public playerControl pc;
	public void TakeDamage(int damage) {
        Debug.Log("this is being called");
       
		health -= damage;
		if(health <= 0) {
			Die();
		}
        
	}
	void Die() {
		//Instantiate(deathEffect, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
