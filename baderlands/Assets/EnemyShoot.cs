using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject horizArrow;
   // public playerControl pc; //to check on the int for the pickup and stuff yk

    //actually this is the wrong class my b imma keep it because idk it looks cool 
    public GameObject arrowPrefab;
    public float speedX;
    public float travelDist;
    public Vector3 startPosition;
    private float distanceMoved;

    void Start()
    {
        InvokeRepeating("LaunchProjectile", 0.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(horizArrow != null)
        {
            if(distanceMoved >= travelDist)
            {
                Destroy(horizArrow);
            }
            else
            {
                horizArrow.transform.position += new Vector3(speedX * Time.deltaTime, 0, 0);
                distanceMoved += speedX * Time.deltaTime;
            }
        }
        else
        {
            distanceMoved = 0;
        }
    }

    void LaunchProjectile()
    {
        horizArrow = Instantiate<GameObject>(arrowPrefab);
        horizArrow.transform.position = startPosition;
    }
}