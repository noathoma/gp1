using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timInput : MonoBehaviour
{
    public float maxSpeed;
    public float speed;
    
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //finds a component of type Animator that's attached to the gameObject that ownss this script
        animator = GetComponent<Animator>();

        if(animator == null)
        {
            Debug.LogError("No animator attached");
        }
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");

        //check if controller is pushed left or right
        if (Mathf.Abs(horizontalAxis) > 0.0001f)
        {
            speed = maxSpeed * horizontalAxis * Time.deltaTime;

            transform.Translate(speed, 0.0f, 0.0f);

            animator.SetBool("isRunning", true);


        }
        else
        {
            animator.SetBool("isRunning", false);
        }

    }
}
