using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHover : MonoBehaviour
{
    public Color hoverC;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if(hit.collider != null) {
            if(hit.collider.gameObject.name == gameObject.name) {
                GetComponent<SpriteRenderer>().color = hoverC;
			} else {
                Debug.Log("Method ++");
                GetComponent<SpriteRenderer>().color = Color.white;
			}
		} else {
            GetComponent<SpriteRenderer>().color = Color.white;
		}
    }
}
