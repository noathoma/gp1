using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//attached to main camera

public class CameraMove : MonoBehaviour
{
    public float max; //maximum speed float value
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");
        float xVel = max * horizontalAxis * Time.deltaTime;//x and y velocity speed calc
        float yVel = max * verticalAxis * Time.deltaTime;
        transform.Translate(xVel, yVel, 0.0f);
    }
}
