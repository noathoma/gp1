using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq; //language integrated query idk if you wanted us to use it but when i was looking up unity button stuff this is what i found sorry

public class ButtonAction : MonoBehaviour
{
    ToggleGroup toggleGroup;
    void Start(){
        toggleGroup = GetComponent<ToggleGroup>();
    }
    public void Send(){
        Toggle toggle = toggleGroup.ActiveToggles().FirstOrDefault();
        Debug.Log(toggle.name + "_" + toggle.GetComponentInChildren<Text>().text);
    }
}
