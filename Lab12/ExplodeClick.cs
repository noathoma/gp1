using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeClick : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject particles;
    private Vector2 mousePos;

    void Start()
    {
        particles.SetActive(false);    
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1)){
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            particles.SetActive(true);
            particles.transform.position = new Vector3(mousePos.x, mousePos.y, -10f);

        }
        if(Input.GetMouseButtonUp(1)){
            particles.SetActive(false);
        }
    }
}
